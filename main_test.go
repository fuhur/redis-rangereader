package redisrangereader

import (
	"github.com/go-redis/redis"
	"io/ioutil"
	"testing"
)

func createClient(keys []string) (redisClient *redis.Client, err error) {
	redisClient = redis.NewClient(&redis.Options{
		Addr:     "localhost:6379",
		Password: "",
		DB:       2,
	})

	for _, k := range keys {
		err := redisClient.Del(k).Err()

		if err != nil {
			return nil, err
		}
	}

	return
}

func TestReaderEmpty(t *testing.T) {
	key := "foo:bar"
	redisClient, err := createClient([]string{key})

	if err != nil {
		t.Error(err)
	}

	reader := NewRedisRangeReader(redisClient, key)
	result, err := ioutil.ReadAll(reader)

	if err != nil {
		t.Error(err)
	}

	defer redisClient.Close()

	if len(result) > 0 {
		t.Error("Empty reader must return empty result", result)
	}
}

func TestReaderNotEmpty(t *testing.T) {
	key := "foo:bar"
	value := "keycontent"

	redisClient, err := createClient([]string{key})

	if err != nil {
		t.Error(err)
	}

	err = redisClient.Del(key).Err()

	if err != nil {
		t.Error(err)
	}

	err = redisClient.RPush(key, value).Err()

	if err != nil {
		t.Error(err)
	}

	reader := NewRedisRangeReader(redisClient, key)
	result, err := ioutil.ReadAll(reader)

	if err != nil {
		t.Error(err)
	}

	defer redisClient.Close()

	if string(result) != value {
		t.Errorf("expected %s, got %s", value, string(result))
	}
}
